#!/usr/bin/env python3

'''nsstk pypi setup.py'''

# Resources:
# https://github.com/pypa/sampleproject/blob/master/setup.py
# https://wiki.python.org/moin/CheeseShopTutorial
# https://python-packaging-user-guide.readthedocs.org/en/latest/distributing.html

import os
from setuptools import setup
#from setuptools import find_packages # Always prefer setuptools over distutils
#from codecs import open # To use a consistent encoding

HERE = os.path.abspath(os.path.dirname(__file__))

# Get the long description from the relevant file
def long_description(fname='DESCRIPTION.rst'):
    '''Gets the long description from the DESCRIPTION.rst file.'''
    ldesc = ''
    if os.path.isfile(fname):
        with open(os.path.join(HERE, fname), encoding='utf-8') as _fh:
            ldesc = _fh.read()
    return ldesc

def pkg_data(pkg_name=None, fname='__init__.py', val=None):
    '''Gets information from pkg/__init__.py (consistency)'''
    if not pkg_name is None and os.path.isdir(pkg_name):
        if not fname is None and os.path.isfile(os.path.join(pkg_name, fname)):
            ffpath = os.path.join(pkg_name, fname)
            with open(ffpath, "r") as _fh:
                for line in _fh.readlines():
                    if line.strip(' \t\r\n') == '':
                        continue
                    if val in line.strip(' \t\r\n-"'):
                        ret = line.strip(' \t\r\n-"')
                        return ret.split('=')[1].strip(' \t\r\n-"')
    raise Exception("Could not retrive package data for '%s' from %s" % (val, os.path.join(HERE, pkg_name, fname)))

setup(
    name='nsstk',

    version=pkg_data('nsstk', "__init__.py", '__version__'),

    # A description of your project
    description='NSS Toolkit (nsstk)',
    long_description=long_description(),

    # The project's main homepage
    url='https://bitbucket.org/noshitsoftware/nsstk',

    # Author details
    #author='No Shit Software',
    #author_email='noshitsoftware@gmail.com',
    author=pkg_data('nsstk', "__init__.py", '__author__'),
    author_email=pkg_data('nsstk', "__init__.py", '__email__'),

    # Choose your license
    #license='GNU GPLv3+',
    license=pkg_data('nsstk', "__init__.py", '__license__'),

    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    # https://pypi.python.org/pypi?:action=list_classifiers
    classifiers=[
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 3 - Alpha',

        # Indicate who your project is intended for
        'Intended Audience :: Developers',
        'Topic :: Software Development',
        'Topic :: Software Development :: Libraries',
        'Topic :: Utilities',

        # Pick your license as you wish (should match "license" above)
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',

        # Specify the Python versions you support here. In particular, ensure
        # that you indicate whether you support Python 2, Python 3 or both.
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
    ],

    # What does your project relate to?
    keywords='utilities tools development libraries nsstk',

    # You can just specify the packages manually here if your project is
    # simple. Or you can use find_packages().
    #packages=find_packages(exclude=['contrib', 'docs', 'tests*']),
    packages=['nsstk.utils', 'nsstk.ver',],

    # List run-time dependencies here. These will be installed by pip when your
    # project is installed. For an analysis of "install_requires" vs pip's
    # requirements files see:
    # https://packaging.python.org/en/latest/technical.html#install-requires-vs-requirements-files
    install_requires=['requests', 'pexpect',],

    # List additional groups of dependencies here (e.g. development dependencies).
    # You can install these using the following syntax, for example:
    # $ pip install -e .[dev,test]
    #extras_require = {
    #'dev': ['check-manifest'],
    #'test': ['coverage'],
    #},

    # http://pythonhosted.org/setuptools/setuptools.html#including-data-files
    # If there are data files included in your packages that need to be
    # installed, specify them here. If using Python 2.6 or less, then these
    # have to be included in MANIFEST.in as well.
    #package_data={
        #'sample': ['package_data.dat'],
    #    'docs': ['docs/*'],
    #}

    # Although 'package_data' is the preferred approach, in some case you may
    # need to place data files outside of your packages.
    # see http://docs.python.org/3.4/distutils/setupscript.html#installing-additional-files
    # In this case, 'data_file' will be installed into '<sys.prefix>/my_data'
    # data_files=[('my_data', ['data/data_file'])],

    # To provide executable scripts, use entry points in preference to the
    # "scripts" keyword. Entry points provide cross-platform support and allow
    # pip to create the appropriate form of executable for the target platform.
    #entry_points={
    #    'console_scripts': [
    #        'foo=my_package.some_module:main_func',
    #        'bar=other_module:some_func',
    #    ],
    #    'gui_scripts': [
    #        'baz=my_package_gui:start_func',
    #    ],
    #    'setuptools.installation': [
    #        'eggsecutable=my_package.some_module:main_func',
    #    ],
    #}
)
