NSS Toolkit(s)
==============

Just small tools and utilities used in the course of software development by No Shit Software.

There is nothing special here. For all intensive purposes, you have reached the end of the interweebs.

Docs are here: <http://nsstk.readthedocs.org/en/latest/>
