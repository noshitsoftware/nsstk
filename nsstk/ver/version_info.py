#!/usr/bin/env python

'''Holds a class for general version information.'''

# System Imports
#import sys, inspect
from collections import namedtuple as ntup
from datetime import datetime as dt
from datetime import timedelta as td

# Package Imports
from nsstk.utils.cgerr import CGErr as cge
#from nsstk.utils.validation import is_int
import nsstk.utils.validation as validate

# From: https://pypi.python.org/pypi?:action=list_classifiers
#Development Status :: 1 - Planning
#Development Status :: 2 - Pre-Alpha
#Development Status :: 3 - Alpha
#Development Status :: 4 - Beta
#Development Status :: 5 - Production/Stable
#Development Status :: 6 - Mature
#Development Status :: 7 - Inactive
# Re-map this as you see fit.
DEV_STATUS = {
    'a' : 'Development Status :: 3 - Alpha',
    'b' : 'Development Status :: 4 - Beta',
    '1' : 'Development Status :: 1 - Planning',
    '2' : 'Development Status :: 2 - Pre-Alpha',
    '3' : 'Development Status :: 3 - Alpha',
    '4' : 'Development Status :: 4 - Beta',
    '5' : 'Development Status :: 5 - Production/Stable',
    '6' : 'Development Status :: 6 - Mature',
    '7' : 'Development Status :: 7 - Inactive',
    1 : 'Development Status :: 1 - Planning',
    2 : 'Development Status :: 2 - Pre-Alpha',
    3 : 'Development Status :: 3 - Alpha',
    4 : 'Development Status :: 4 - Beta',
    5 : 'Development Status :: 5 - Production/Stable',
    6 : 'Development Status :: 6 - Mature',
    7 : 'Development Status :: 7 - Inactive',
    }

# Please note that if you use a different format than the defaults,
# You should edit this to reflect your respective formats, or pass them in with kwargs.
VER_TYPE = {
    'mm', 'Major.Minor',
    'mmm', 'Major.Minor.Micro',
    'mmmm', 'Major.Minor.Micro.Misc',
    'date_v1', 'YYYY.MM.DD.Misc', # Misc Unix_timestamp[day_diff][0:3]
    'date_v2', 'YYYY.Julian.Micro', # Micro is Unix_timestamp[day_diff][0:3]
    }

class VersionInfo(object):
    '''Provides a general version-info object to pass around.

    See PEP 440 for details. (This implementation is incomplete, and date versioning is different.)

    Also, please note, all releases are assumed "beta" by default, unless otherwise specified.
    This is given the nature of things changing so much, so often at the time of writing this versioning software.

    Accepts:
    * version_str = (str), following one of the parsable formats.
    * kwargs = (dict) of keyword arguments matching class variables, as defined.

    Parsable Formats:

    * Major.Minor
    * Major.Minor.Micro
    * Major.Minor.Micro.Misc
    * (Date) YYYY.MM.DD.Misc
    * (Date) YYYY.Julian.Micro

    Internal Variables (kwargs):

    * major
    * minor
    * micro
    * misc
    * vstr -> Maps to the same as (accepts) version_str
    * alpha, or _alpha for alpha development status.
    * beta, or _beta for beta development status.
    * rc, or _rc for "c" or release-candidate development status.
    * pre, or _pre for pre-release development status (testing / development).
    * post, or _post for post-release development status (usually quick fixes).
    * dev, or _dev for development status (not to be mixed with _pre or _post).
    * final, or _final for final status (clears all of the above flags and gives you a RAW version number).
    * dev_status, or _dev_status -- as it maps to setup.py classifiers.
    '''

    _kwa = [
        'major', 'minor', 'micro', 'misc', 'vstr',
        'alpha', '_alpha', 'beta', '_beta', 'rc', '_rc',
        'pre', '_pre', 'post', '_post', 'dev', '_dev', 'final', '_final',
        'dev_status', '_dev_status',]
    #: vstr is the output version string.
    vstr = None
    major = None
    minor = None
    micro = None
    misc = None
    _alpha, _beta, _rc = False, False, False
    _pre, _post = False, False
    _dev, _final = False, False
    _dev_status, _detected_version_type = None, None

    @property
    def alpha(self):
        '''Specifies this version as alpha.'''
        return self._alpha
    @alpha.setter
    def alpha(self, is_alpha=False):
        '''Specifies this version as alpha.'''
        if is_alpha:
            if self._beta:
                self._beta = False
            if self._rc:
                self._rc = False
            if self._final:
                self._final = False
            self._alpha = True
            self._dev_status = DEV_STATUS['a']
        else:
            self._alpha = False

    @property
    def beta(self):
        '''Specifies this version as beta.'''
        return self._beta
    @beta.setter
    def beta(self, is_beta=False):
        '''Specifies this version as beta.'''
        if is_beta:
            if self._alpha:
                self._alpha = False
            if self._rc:
                self._rc = False
            if self._final:
                self._final = False
            self._dev_status = DEV_STATUS['b']
            self._beta = True
        else:
            self._beta = False

    @property
    def rcandidate(self):
        '''Specifies this version as a release candidate.'''
        return self._rc
    @rcandidate.setter
    def rcandidate(self, is_rc=False):
        '''Specifies this version as a release candidate.'''
        if is_rc:
            if self._alpha:
                self._alpha = False
            if self._beta:
                self._beta = False
            if self._final:
                self._final = False
            self._rc = True
        else:
            self._rc = False

    @property
    def pre(self):
        '''Specifies this version as a pre-release.'''
        return self._pre
    @pre.setter
    def pre(self, is_pre=False):
        '''Specifies this version as a pre-release.'''
        if is_pre:
            if self._post:
                self._post = False
            if self._final:
                self._final = False
            self._pre = True
        else:
            self._pre = False

    @property
    def post(self):
        '''Specifies this version as a post-release.'''
        return self._post
    @post.setter
    def post(self, is_post=False):
        '''Specifies this version as a post-release.'''
        if is_post:
            if self._pre:
                self._pre = False
            if self._final:
                self._final = False
            self._post = True
        else:
            self._post = False

    @property
    def dev(self):
        '''Specifies this is a development version.'''
        return self._dev
    @dev.setter
    def dev(self, is_dev=False):
        '''Specifies this is a developement version.'''
        if is_dev:
            if self._final:
                self._final = False
            self._dev = True
        else:
            self._dev = False

    @property
    def final(self):
        '''Specifies this is a final version.'''
        return self._final
    @final.setter
    def final(self, is_final=False):
        '''Specifies this is a final version.'''
        if is_final:
            if self._alpha:
                self._alpha = False
            if self._beta:
                self._beta = False
            if self._rc:
                self._rc = False
            if self._pre:
                self._pre = False
            if self._post:
                self._post = False
            if self._dev:
                self._dev = False
            if self._pre:
                self._pre = False
            self._dev_status = DEV_STATUS['5']
            self._final = True
        else:
            self._final = False

    @property
    def dev_status(self):
        '''Specifies the development status (according to setup.py classifiers).'''
        return self._dev_status
    @dev_status.setter
    def dev_status(self, val=None):
        '''Specifies the development status (according to setup.py classifiers).'''
        if val in DEV_STATUS.keys():
            self._dev_status = DEV_STATUS[val]
        else:
            if self._dev_status is None:
                self._dev_status = DEV_STATUS['b']

    @property
    def version_type(self):
        '''Returns the detected version type.'''
        return self._detected_version_type

    def __init__(self, version_str=None, **kwargs):
        self.error = cge.error(self)
        if not version_str is None:
            self.vstr = version_str
            self.parse_ver_str()
        else:
            if 'vstr' in kwargs:
                self.vstr = kwargs['vstr']
            else:
                self._parse_kwa(kwargs)
        if self.vstr is None and (self.major is None and self.minor is None):
            msg = "VersionInfo expect you pass in a version string for parsing,"
            msg += "or a kwargs dict with at least major and minor."
            raise self.error(msg)

    def _parse_kwa(self, kwargs):
        '''Parses the passed kwargs into local self variables.'''
        for item in self._kwa:
            if item in kwargs:
                self.__setattr__(item, kwargs[item])

    def __str__(self):
        '''Returns a string representing the Major.Minor[.Micro[.Misc]] represented internally.'''
        ret = ''
        if not self.major is None and not self.minor is None:
            ret = '%s.%s' % (self.major, self.minor)
            if not self.micro is None:
                ret += '.'+self.micro
                if not self.misc is None:
                    ret += '.'+self.misc
                    # Returns Major.Minor.Micro.Misc
                    return ret
                else:
                    # Returns Major.Minor.Micro
                    return ret
            else:
                # Returns Major.Minor
                return ret
        else:
            msg = "Major and/or Minor versions are None. "
            msg += "This class only supports Major.Minor versioning, at a minimum."
            raise self.error(msg)

    def __iter__(self):
        # If you're reading this,
        # and you know how detect the calling sequence type,
        # such as tuple(class), dict(class), or list(class) ...
        # I would appreciate knowing how to do this.
        # Multiple inheretence of base types is prohibited given many conflicts.
        # There are many complicated ways to handle this, ... wasteful ... (probably a better way)
        # When I try, the lowest stack trace frame is simply <module> without definition.
        # It would be nice just simply to call list(class) and get [major, minor, micro, misc, vstr]
        # As if this iteration, __iter__ returns a an implict dict of [['major', 'x'], ['minor', 'y'], ... ]
        lst = []
        lst.append(['major', self.major])
        lst.append(['minor', self.minor])
        lst.append(['micro', self.micro])
        lst.append(['misc', self.misc])
        lst.append(['vstr', self.vstr])
        lst.append(['alpha', self._alpha])
        lst.append(['beta', self._beta])
        lst.append(['rc', self._rc])
        lst.append(['pre', self._pre])
        lst.append(['post', self._post])
        lst.append(['dev', self._dev])
        lst.append(['final', self._final])
        return iter(lst)

    def get_namedtuple(self):
        '''Returns a named tuple of the version information.'''
        _nt = ntup('VersionInfo', 'major minor micro misc vstr alpha beta rc pre post dev final')
        return _nt(major=self.major, minor=self.minor, micro=self.micro, misc=self.misc, vstr=self.vstr, alpha=self._alpha, beta=self._beta, rc=self._rc, pre=self._pre, post=self._post, dev=self._dev, final=self._final)

    def _detect_pre_post(self):
        '''Internal detection of pre or post release from version string.'''
        if 'pre' in self.vstr and 'post' in self.vstr:
            msg = "You have both 'pre' and 'post' in your version string? "
            msg += "This class doesn't support that."
            raise self.error(msg)
        else:
            if 'pre' in self.vstr:
                self.pre = True
            elif 'post' in self.vstr:
                self.post = True

    def _detect_alpha_beta_candidate(self):
        '''Internal detection of alpha, beta or release candidate (c or rc) from version string.'''
        if 'a' in self.vstr or 'b' in self.vstr or 'c' in self.vstr:
            msg = "This class does not support specifying more than one of alpha, beta or release-candidate."
            if 'a' in self.vstr and ('b' in self.vstr or 'c' in self.vstr):
                pass
            elif 'b' in self.vstr and ('a' in self.vstr or 'c' in self.vstr):
                pass
            elif 'c' in self.vstr and ('a' in self.vstr or 'b' in self.vstr):
                pass
            else:
                msg = ''
            if msg != '':
                raise self.error(msg)
            else:
                if 'a' in self.vstr:
                    self.alpha = True
                elif 'b' in self.vstr:
                    self.beta = True
                elif 'c' in self.vstr:
                    self.rcandidate = True
                else:
                    msg = "Uh-oh, spagetti-oh. Something must be horribly wrong."
                    raise self.error(msg)

    def _verify_major_ver(self):
        '''Internal Major version verification routine.'''
        if len(self.major) > 4:
            msg = "This software only does date-based major versioning, or major versions < 100."
            raise self.error(msg)
        elif len(self.major) == 4:
            if not validate.is_int(self.major):
                msg = "Major version integer conversion failed for: '%s'" % (self.major)
                raise self.error(msg)
            elif int(self.major) >= 1970 and int(self.major) <= dt.utcnow().year+td(hours=12):
                return True
            else:
                msg = "Invalid Major date version: '%s'" % (self.major)
                raise self.error(msg)
        elif len(self.major) == 3:
            msg = "This software does not support major versioning in the 100's!"
            raise self.error(msg)
        elif len(self.major) in [1, 2]:
            if self.is_int(self.major) and int(self.major) >= 0:
                return True
            else:
                msg = "Major version cannot be less than zero (0). You gave: '%s'" % (self.major)
                raise self.error(msg)
        else:
            msg = "Uh-oh, spagetti-oh. Something must be horribly wrong."
            raise self.error(msg)

    def _verify_minor_version(self, amt=0):
        '''Internal Minor version verification routine.

        Accepts:
        * amt = (int) amount to increment the minor version by.
        '''
        if len(str(self.minor)) > 3:
            msg = "This software does not support minor versioning above 999. "
            msg += "AND, This software will not auto-increment major versioning."
            raise self.error(msg)
        elif len(str(self.minor)) <= 3:
            if not validate.is_int(self.minor) or int(self.minor) < 0:
                msg = "Minor version integer conversation failed for: '%s'" % (self.minor)
                raise self.error(msg)
            else:
                if not amt is None and validate.is_int(amt):
                    if int(amt) < 0:
                        msg = "This software does not support reverse versioning! Minor version amt='%s'" % (amt)
                        raise self.error(msg)
                    elif int(self.minor)+int(amt) >= 1000:
                        msg = "This software does not support minor versioning above 999. "
                        msg += "AND, This software will not auto-increment major versioning."
                        raise self.error(msg)
                return True
        else:
            msg = "Uh-oh, spagetti-oh. Something must be horribly wrong."
            raise self.error(msg)

    def _verfiy_micro_version(self, amt=0):
        '''Internal Micro version verification routine.

        Accepts:

        * amt = (int) amount to increment the minor version by...
        '''
        if len(str(self.micro)) > 3:
            msg = "This software does not support micro versioning above 999."
            raise self.error(msg)
        elif len(str(self.micro)) <= 3:
            pass
        else:
            msg = "Uh-oh, spagetti-oh. Something must be horribly wrong."
            raise self.error(msg)

    def parse_ver_str(self):
        '''Parses a passed-in version string.'''
        if not '.' in self.vstr:
            msg = "A version string must be dotted. '%s' is an invalid version string." % (self.vstr)
            raise self.error(msg)
        else:
            tmp = self.vstr.split('.')
            if len(tmp) > 4:
                msg = "This class does not support more than Major.Minor.Micro.Misc versioning. "
                msg += "'%s' is invalid, having more than 3 '.' separators."
                raise self.error(msg)
            self.major = tmp[0]
            self._verify_major_ver()
            self.minor = tmp[1]
            self._verify_minor_version()
            if len(tmp) >= 3:
                self.micro = tmp[2]
                self._verfiy_micro_version()
            if len(tmp) == 4:
                self.misc = tmp[3]
                self._verify_misc_version()
        self._detect_alpha_beta_candidate()
        self._detect_pre_post()
        if 'dev' in self.vstr:
            self.dev = True

    @classmethod
    def is_int(cls, val=None):
        '''Just a basic internal (redirection) method to check if something is or isn't an int.'''
        return validate.is_int(val)

    def increment_major(self):
        '''This IS NOT supported. Increment it yourself.'''
        raise self.error(self.increment_major.__doc__)

    def increment_minor(self, amt=None):
        '''Increments the minor version number by amt if specified, otherwise 1.'''
        if not amt is None and type(amt) in [int, str]:
            pass
        else:
            mlen = len(self.minor)
            zpad = '0'*mlen
            new_minor = int(self.minor)+1

if __name__ == '__main__':
    print("\n\t%s\n" % (__doc__))
