#!/usr/bin/env python3

'''Updates a YYYY.MM.DD{abc}date_delta version format.'''

# System Imports
import os,  sys
from datetime import datetime as dt
from calendar import timegm as tgm
#from datetime import timedelta

# 3rd Party Imports
#import pexpect

def parse_pip_search_data(pip_data=None):
    '''Searches PyPi using pip for the latest version of the specified package.'''
    if not pip_data is None:
        for line in pip_data.strip(' \t\r\n').split('\n'):
            tst = line.strip(' \t\r\n')
            if 'INSTALLED' in tst:
                if '(latest)' in tst:
                    return tst.split(':')[1].split('(')[0].strip(' \t\r\n-"')
            elif 'LATEST' in tst:
                return tst.split(':')[1].strip(' \t\r\n-"')
    return None

def date_delta_version_split(ver=None,  lst=None):
    '''Splits an existing version and returns the tuple.'''
    if not ver is None and not lst is None and type(ver) == str and type(lst) == list:
        if not '.' in ver:
            return ver
        vtmp = ver.split('.')
        for xyz in lst:
            if xyz in vtmp[-1]:
                tmp = vtmp[-1].split(xyz)
                vtmp[-1] = tmp[0]
                vtmp.append(xyz)
                vtmp.append(tmp[1])
                return vtmp
    return ver

def date_delta_version_update(pkg=None, abcxyz='b'):
    '''
    Updates a YYYY.MM.DD{abc}date_delta version format.

    * pkg = string name of the package
    * abcxyz = should be some version appendage such as {a, b, c, dev, pre, post, rc}, as defined in PEP 440.

    If you have the following: ::

        >>> import __init__
        >>> __init__.__version__
        2014.12.15a###

    Where ###### is some delta number (time diff in seconds).

    If today's date is something like 2014-12-16,
    then the new version will be:
    2014.12.16{abcxyz}{lmno},
    where {lmno} will be the first 3 digits of the difference between unix timestamps of now versus start of the day.
    '''
    if not pkg is None and type(pkg) == str:
        pkg_init = os.path.join(pkg, '__init__.py')
        if os.path.isfile(pkg_init):
            utcnow = dt.utcnow()
            ver_str = '__version__ = "%s.%s.%s{tier}{delta}"\n' % (utcnow.year,  utcnow.month,  utcnow.day)
            utcnow_tgm = tgm(utcnow.timetuple())
            dstart = dt(utcnow.year,  utcnow.month,  utcnow.day)
            dstart_tgm = tgm(dstart.timetuple())
            delta = str(utcnow_tgm-dstart_tgm)[0:3]
            ver_str = ver_str.format(tier=abcxyz, delta=delta)
            pkg_lines = []
            # Read existing lines.
            abpath = os.path.abspath(pkg_init)
            with open(pkg_init,  "r") as _fh:
                for line in _fh.readlines():
                    # Replace the existing version string.
                    if '__version__' in line:
                        print("Replacing current version: %s" % (line.strip(' \t\r\n')))
                        print("\tIn file: '%s'" % (abpath))
                        pkg_lines.append(ver_str)
                        print("With: %s" % (ver_str))
                    else:
                        pkg_lines.append(line)
            # Write the new file.
            with open(pkg_init, "w") as _fh:
                for line in pkg_lines:
                    _fh.write(line)

def date_delta_version_update_help():
    '''Provides date_delta_version_update help.'''
    print("\n\t%s\n" % (__doc__))
    print("This program requires exactly 2 argments.")
    print("\tpython -m nsstk.utils.date_delta_version_update {pkg} {a, b, c, dev, post, pre, rc, etc}")
    print("Where {pkg} is the name of the package.")
    print("This should be run in the directory below,")
    print("\tand there needs to be an __init__.py in the package root directory!!!")
    print("Where {a, b, c, ...} is the package tier (see PEP 440).")
    print("\n")
    exit(1)

if __name__ == '__main__':
    #print("\n\t%s\n" % (__doc__))
    ARGS = sys.argv
    if len(ARGS) != 3:
        date_delta_version_update_help()
    else:
        if not os.path.isdir(ARGS[1]):
            date_delta_version_update_help()
        elif not ARGS[2] in ['a', 'b', 'c', 'd', 'dev', 'pre', 'post', 'rc']:
            date_delta_version_update_help()
        else:
            date_delta_version_update(ARGS[1], ARGS[2])
