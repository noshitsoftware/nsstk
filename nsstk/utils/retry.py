#!/usr/bin/env python3

import time
import math

"""
Retry Decorator
From: https://wiki.python.org/moin/PythonDecoratorLibrary#Retry
"""

# Retry decorator with exponential backoff
def retry(tries, delay=3, backoff=2):
    '''Retry Decorator

    This is pulled directly from the following resource:
    https://wiki.python.org/moin/PythonDecoratorLibrary#Retry

    * tries = You must specify a number of attempts to try.
    * delay = In seconds *(initial wait period)*
    * backoff = In seconds *(length of subsequent delays)*

    Usage ::

        from nsstk.utils.retry import retry
        @retry(3, 2, 1)
        def some_method():
            do_something = False
            return do_something

    The above example will try 3 times and finally fail.
    The first attempt happens immediatly.
    The second attempt happens 2 seconds after failure.
    The third attempt happens 2 seconds after the 2nd failure (4 seconds from the first attempt).

    If 'True' is returned, no more attempts are made.

    Example 2 ::

        @retry(3, 15, 2)

    Ex.2 will wait 15 seconds, and then 30 seconds, for a total of 45 seconds of wait-time across all 3 failures.
    (Or return True if any of the attempts first return True.)
    '''
    if backoff <= 1:
        raise ValueError("backoff must be greater than 1")

    tries = math.floor(tries)
    if tries < 0:
        raise ValueError("tries must be 0 or greater")

    if delay <= 0:
        raise ValueError("delay must be greater than 0")

    def deco_retry(f):
        def f_retry(*args, **kwargs):
            mtries, mdelay = tries, delay # make mutable
            rv = f(*args, **kwargs) # first attempt
            while mtries > 0:
                if rv is True: # Done on success
                    return True
                mtries -= 1      # consume an attempt
                time.sleep(mdelay) # wait...
                mdelay *= backoff  # make future wait longer
                rv = f(*args, **kwargs) # Try again
            return False # Ran out of tries :-(
        return f_retry # true decorator -> decorated function
    return deco_retry  # @retry(arg[, ...]) -> true decorator

if __name__ == '__main__':
    print("\n\t%s\n" % (__doc__))
