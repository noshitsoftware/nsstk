#!/usr/bin/env python3

'''Provides a generic, customizable exception class.'''

import inspect

class CGErr(Exception, object):
    '''
    * msg = Your custom message (str)

    CGErr - Custom Generic Error

    Provides a generic, customizable exception class.
    Makes it look like the exception came from a custom exception class.

    Usage ::

        from nsstk.utils.cgerr import CGErr as cge

        class Something(object):

            def __init__(self, whatever):
                self.error = cge.error(self)

            def some_method(self, msg=None):
                raise self.error(msg)

        # This also works.
        class AnotherWay(object):

            error = None

            def another_method(self, msg=None):
                self.error = cge.error(self)
                raise self.error(msg)

        # I am a customization junkie
        class YouCanDoThisToo(object):

            def really_custom(self, msg=None, nom='CustomJuCanDoIt'):
                err = cge.error(self, nom)
                raise err(msg)

    Output ::

        >>> var = Something()

        >>> var.some_method()
        Traceback (most recent call last)
        File "<stdin>", line 1, in <module>
        File "<stdin>", line 5, in some_method
        __main__.SomethingErr: Unspecified error. See stack trace.

        >>> var.some_method("This error doesn't look like it came from CGErr")
        Traceback (most recent call last)
        File "<stdin>", line 1, in <module>
        File "<stdin>", line 5, in some_method
        __main__.SomethingErr: This error doesn't look like it came from CGErr

        # __main__ If from console, otherwise,
        # looks like it comes from the source module stack trace.
        # It *should not* look like: nsstk.utils.CGErr.SomethingErr
        # It *should* look like: your.method.stack.SomethingErr

        >>> var = YouCanDoThisToo()
        >>> var.really_custom("Customization in action.")
        Traceback (most recent call last)
        File "<stdin>", line 1, in <module>
        File "<stdin>", line 4, in really_custom
        __main__.YouCanDoItCustomJuCanDoIt: Customization in action.
    '''

    _msg = "Unspecified error. See stack trace."
    _nom = 'Err'

    @classmethod
    def error(cls, obj=None, nom=None):
        '''
        * msg = Your custom message (str)
        * nom = If you want to re-name the custom error class.

        See above usage example.

        This is the assignment method: ::

            self.error = cge.error(self)

        It requires the use of a self assignment within a class method block (def or __init__).
        It cannot be declared in a non-self block. *self* MUST BE RESOLVED!

        You pass *self* as the object you're assigning the custom class to.

        Instead of... ::

            class SomethingErr(Exception):

                def __init__(self, msg):
                    super(SomethingErr, self).__init__(msg)

            class Something(object):

                def some_method(self, msg=None):
                    raise SomethingErr(msg)

        This does it for you... ::

            from nsstk.utils.cgerr import CGErr as cge

            class Something(object):

                def __init__(self, whatever):
                    self.error = cge.error(self)

                def some_method(self, msg=None):
                    raise self.error(msg)

                def another_method(self, msg=None, nom='SuperDuperExtraSpecialException'):
                    local_customized_err = cge.error(self, nom)
                    raise local_customized_err(msg)

        This @classmethod is automatically deleted once it's assigned.
        '''
        if not obj is None and not obj.__class__.__name__ in ['type', 'function']:
            if not nom is None and isinstance(nom, str) and len(nom) <= 30:
                cls._nom = nom
            new_type = type(obj.__class__.__name__+cls._nom, cls.__bases__, dict(cls.__dict__))
            new_type.__module__ = obj.__module__
            del new_type.error
            new_type.__doc__ = 'Exception for %s' % (obj.__class__.__name__)
            return new_type
        else:
            return Exception

    def __init__(self, msg=None):
        if not msg is None:
            if type(msg) == str:
                self._msg = msg
            else:
                self._msg = repr(msg)
        self.__name__ = inspect.stack()[2][3]+self._nom
        # Pylint doesn't like this, but it works.
        super(Exception, self).__init__(self._msg)

if __name__ == '__main__':
    print("\n\t%s\n" % (__doc__))
