#!/usr/bin/env python

'''Basic type validation routines'''

def is_int(val=None):
    '''Verifies a variable converts to an integer successfully'''
    if not val is None:
        try:
            int(val)
            return True
        except ValueError:
            return False
    else:
        return False

if __name__ == '__main__':
    print("\n\t%s\n" % (__doc__))
